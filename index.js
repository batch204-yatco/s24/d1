// alert("B204!");

// Exponent Operator 

// Using exponent operator
const firstNum = 8 ** 3;
console.log(firstNum);

// Before ES6 updates
/*
	Syntax:
		Math.pow(base, exponent);
*/
const secondNum = Math.pow(8, 3);
console.log(secondNum);

// Template Literals
/*
	Allows to write strings without using concatenation operator (+)
	${} - Placeholder when using template literals
*/

let name = "John";
// Pre-Template Literal String
// Use single quote ('')
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Meassage without template literals: " + message);

// Strings using Template Literals
// Using backticks(``)
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

//Multi-line using Template Literals 
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 3 with the solution of ${firstNum}.
`
console.log(anotherMessage);

let anotherMessage1 = "\n " + name + " attended a math competition." + "\n He won it by solving the problem 8 ** 3 with the solution of " + firstNum;
console.log(anotherMessage1);

const interestRate = .1 
const principal = 1000
console.log(`The interest on your savings is: ${principal * interestRate}`);

// Array Destructuring 
/*
	Allows us to unpack elements in arrays into distinct variables
	Allows us to name array elements with variables instead of using index numbers

	Syntax:
		let/const [variableName1, variableName2, variableName3] = array

*/
const fullName = ["Joe", "Dela", "Cruz"];

// Pre-Array Destructuring 
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Array Destructuring 
// Each element already has a variable name
// Ordering matters
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}!`)

// Object Destructuring 
/*
	Allows us to unpack properties of objects into distinct variables
	Shortens syntax for accessing properties from objects

	Syntax:
		let/const {propertyName, propertyName} = object
*/

const person = {
	givenName: "Raf",
	maidenName: "Vien",
	familyName: "Santillan"
};

// Pre-Object Destructuring 
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Object Destructuring
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// Arrow Functions
/*
	- Compact alternative syntax to traditional functions

	Syntax:
		let/const variableName = () => {
			console.log()
		}

*/

// Function expression in ES6
const hello = () => {
	console.log("Hello World");
}

hello();

// Function Declaration before ES6
function hello1 () {
	console.log("Hello World");
}

hello1();

// Pre-Arrow Function and Template Literals
/*
	Syntax:
		function functionName (parameterA, parameterB){
			console.log()
		}
*/

// function printFullName(firstName, middleInitial, lastName){
// 	console.log(firstName + " " + middleInitial + ". " + lastName);

// }

// printFullName("Michael", "P", "Calasin");

// Mini Activity
/*
	Convert function into arrow function
*/


const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
};

printFullName("Michael", "P.", "Calasin");

// Arrow functions using Array Iteration method
// Pre-arrow function
const students = ["Aron", "Daphne", "Edvic", "Angelo", "Roxanne"];

students.forEach(function(student){
	console.log(student + " is a student.");
});

// Arrow Function
students.forEach((student) => console.log(`${student} is a student`));
students.forEach((student) => console.log(`${student} is a student`));

// Implicit return statement
// Pre-arrow Function
function add(x,y){
	return x + y
}

let total = add(100, 200);
console.log(total);

// Arrow function
const add1 = (x, y) => x + y;

let total1 = add1(100, 200); 
console.log(total1);

console.log(add1(100, 500));

// Return 
const add2 = (x, y) => {
	return x + y;
};

let totalAdd = add2(1, 1);
console.log(totalAdd);


// Default Function Argument Value
const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet());
console.log(greet("Mikki"));

function greet1 (name = "User", age = 18){
	return `Good morning, ${name}! Age: ${age}`;
}

console.log(greet1());
console.log(greet("Mikki", 17));

// Class-Based Object Blueprint
/*
	Allow creation/instantiation of object using classes as blueprints
	
	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB)
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

/*
	function Car(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;	
	}
*/

// Pre-ES6
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// ES6
const myNewCar = new Car("Ford", "Ranger Raptor", 2022);
console.log(myNewCar);

